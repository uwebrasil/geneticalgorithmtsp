﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GAF;
using GAF.Operators;
using GAF.Extensions;
using GAF.Threading;
using System.IO;

namespace GAFTSP
{
    class Program
    {
        public static int MAX_GENERATION = 20000;
        // Anzahl der Staedte
        public static int n;
        // label-x-y
        public static List<dynamic> label_x_y = new List<dynamic>();

        //public static List<City>  _cities = new List<City>();
        private static List<City> _cities;
        private static List<City2> _cities2;
        static void Main(string[] args)
        {
            ReadFromFile("51_1");
            //get our cities
            //_cities = CreateCities().ToList();
            //_cities = CreateCities();
            _cities2 = CreateCities2();

            //create the population of chromosomes each with 16 genes
            var population = new Population(n);

            //populate the population with 100 chromosomes
            for (var p = 0; p < 100; p++)
            {
                var chromosome = new Chromosome();
                for (var g = 0; g < n; g++)
                {
                    chromosome.Genes.Add(new Gene(g));
                }

                //mix each chromosome up and add to the population
                chromosome.Genes.Shuffle();
                population.Solutions.Add(chromosome);
            }

            //create the elite operator
            var elite = new Elite(10);
            var crossover = new Crossover(3.0)
            {
                CrossoverType = CrossoverType.DoublePointOrdered
            };

            //create the SwapMutate operator
            var mutate = new SwapMutate(0.2);

            //run the GA
            var ga = new GeneticAlgorithm(population, CalculateFitness);

            //subscribe to the generation and run complete events§
            ga.OnGenerationComplete += ga_OnGenerationComplete;
            ga.OnRunComplete += ga_OnRunComplete;

            //add the operators
            ga.Operators.Add(elite);
            ga.Operators.Add(crossover);
            ga.Operators.Add(mutate);

            //run the GA
            ga.Run(Terminate);
            Console.ReadLine();
        }
        //private static IEnumerable CreateCities()
        private static List<City> CreateCities()
        {
            var cities = new List<City>();
            cities.Add(new City("Birmingham", 52.486125, -1.890507));
            cities.Add(new City("Bristol", 51.460852, -2.588139));
            cities.Add(new City("London", 51.512161, -0.116215));
            cities.Add(new City("Leeds", 53.803895, -1.549931));
            cities.Add(new City("Manchester", 53.478239, -2.258549));
            cities.Add(new City("Liverpool", 53.409532, -3.000126));
            cities.Add(new City("Hull", 53.751959, -0.335941));
            cities.Add(new City("Newcastle", 54.980766, -1.615849));
            cities.Add(new City("Carlisle", 54.892406, -2.923222));
            cities.Add(new City("Edinburgh", 55.958426, -3.186893));
            cities.Add(new City("Glasgow", 55.862982, -4.263554));
            cities.Add(new City("Cardiff", 51.488224, -3.186893));
            cities.Add(new City("Swansea", 51.624837, -3.94495));
            cities.Add(new City("Exeter", 50.726024, -3.543949));
            cities.Add(new City("Falmouth", 50.152266, -5.065556));
            cities.Add(new City("Canterbury", 51.289406, 1.075802));

            return cities;
        }
        private static List<City2> CreateCities2()
        {
            var cities = new List<City2>();
            foreach (var c in label_x_y) 
            {
                cities.Add(new City2(c.label, c.x, c.y));
            }
            
            return cities;
        }
        public class City
        {
            public City(string name, double latitude, double longitude)
            {
                Name = name;
                Latitude = latitude;
                Longitude = longitude;
            }

            public string Name { set; get; }
            public double Latitude { get; set; }
            public double Longitude { get; set; }

            /// <summary>
            /// Returns the distance in Km between this City and the specified location.
            /// <summary>
            public double GetDistanceFromPosition(double latitude, double longitude)
            {
                var R = 6371; // radius of the earth in km
                var dLat = DegreesToRadians(latitude - Latitude);
                var dLon = DegreesToRadians(longitude - Longitude);
                var a =
                            System.Math.Sin(dLat / 2) * System.Math.Sin(dLat / 2) +
                            System.Math.Cos(DegreesToRadians(Latitude)) * System.Math.Cos(DegreesToRadians(latitude)) *
                            System.Math.Sin(dLon / 2) * System.Math.Sin(dLon / 2)
                            ;
                var c = 2 * System.Math.Atan2(System.Math.Sqrt(a), System.Math.Sqrt(1 - a));
                var d = R * c; // distance in km
                return d;
            }
            private static double DegreesToRadians(double deg)
            {
                return deg * (System.Math.PI / 180);
            }
            public byte[] ToBinaryString()
            {
                var result = new byte[6];
                return result;
            }
        }

        public static bool Terminate(Population population, int currentGeneration, long currentEvaluation)
        {
            return currentGeneration > MAX_GENERATION;
        }

        public static double CalculateFitness(Chromosome chromosome)
        {
            var distanceToTravel = CalculateDistance(chromosome);
            return 1 - distanceToTravel / 10000;
        }

        private static double CalculateDistance(Chromosome chromosome)
        {
            var distanceToTravel = 0.0;
            City2 previousCity = null;

            //run through each city in the order specified in the chromosome
            foreach (var gene in chromosome.Genes)
            {
                //var currentCity = _cities[(int)gene.RealValue];
                var currentCity = _cities2[(int)gene.RealValue];

                if (previousCity != null)
                {
                   // var distance = previousCity.GetDistanceFromPosition(currentCity.Latitude,
                    //                          currentCity.Longitude);
                    var distance = previousCity.GetDistanceFromPosition2(currentCity.X,
                                              currentCity.Y);

                    distanceToTravel += distance;
                    //Console.WriteLine(string.Format("Chromosome: {0} {1}Km between {2} and {3}.",chromosome.Id, distance, currentCity.Name, previousCity.Name));
                }

                previousCity = currentCity;
            }

            return distanceToTravel;
        }

        static void ga_OnRunComplete(object sender, GaEventArgs e)
        {
            var fittest = e.Population.GetTop(1)[0];
            foreach (var gene in fittest.Genes)
            {
                Console.Write(" "+_cities2[(int)gene.RealValue].Name);
            }
        }

        private static void ga_OnGenerationComplete(object sender, GaEventArgs e)
        {
            var fittest = e.Population.GetTop(1)[0];
            var distanceToTravel = CalculateDistance(fittest);
            Console.WriteLine(string.Format("Generation: {0}, Fitness: {1}, Distance: {2}", e.Generation, fittest.Fitness, distanceToTravel));
            if (distanceToTravel > 2000)
            {
                Console.WriteLine("new start");                
            }
        }

        //////////////////////////////////////////////////////////////////////////////////////////////////
        public class City2
        {
            public City2(string name, double x, double y)
            {
                Name = name;
                X = x;
                Y = y;
            }

            public string Name { set; get; }
            public double X { get; set; }
            public double Y { get; set; }

            /// <summary>
            /// Returns the distance in Km between this City and the specified location.
            /// <summary>
            public double GetDistanceFromPosition2(double caller_x, double caller_y)
            {

                var d = System.Math.Sqrt( System.Math.Pow( X - caller_x,2) + System.Math.Pow( Y - caller_y,2) );
                return d;
            }
                       
        }
       
        private static double CalculateDistance2(Chromosome chromosome)
        {
            var distanceToTravel = 0.0;
            City2 previousCity = null;

            //run through each city in the order specified in the chromosome
            foreach (var gene in chromosome.Genes)
            {
                var currentCity = _cities2[(int)gene.RealValue];

                if (previousCity != null)
                {
                    var distance = previousCity.GetDistanceFromPosition2(currentCity.X,
                                              currentCity.Y);

                    distanceToTravel += distance;
                    //Console.WriteLine(string.Format("Chromosome: {0} {1}Km between {2} and {3}.",chromosome.Id, distance, currentCity.Name, previousCity.Name));
                }

                previousCity = currentCity;
            }

            return distanceToTravel;
        }

       
        //////////////////////////////////////////////////////////////////////////////////////////////////
        // Read in lines from file
        public static void ReadFromFile(string f)
        {
            String loc = ".\\data\\tsp_"+f;
            int i = 0;
            foreach (string line in File.ReadLines(loc))
            {
                if (i++ == 0) { n = Convert.ToInt32(line); continue; }
                var s = line.Split();
                label_x_y.Add( new { label = (i-1).ToString(),
                                     x = Convert.ToInt32(s[0]),
                                     y = Convert.ToInt32(s[1])
                                   });                  
            }
        }
    }
}
